﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
     <asp:Panel ID="Panel1" runat="server">
    <form id="form1" runat="server">
    <div runat="server" id="userinfo">
    </div>
       
        <h3 > Input your Image information</h3>
        
        <br />
       

       
        Choose Image <asp:FileUpload ID="imageurl" runat="server" />
        <asp:TextBox ID="alttext" runat="server" placeholder="alttext"></asp:TextBox>
        
        <asp:TextBox ID="memetext" runat="server" placeholder="meme text"></asp:TextBox>
        <asp:TextBox ID="x" runat="server" value="100" pattern="[0-9]+"></asp:TextBox>
        <asp:TextBox ID="y" runat="server" value="200" pattern="[0-9]+"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="submit image" OnClick="Button1_Click"/>
        <asp:Label ID="displaytext" runat="server" Text=""></asp:Label>
        
    </form>
     </asp:Panel>
    <asp:Panel ID="Panel2" runat="server">
        <h3> You Need to be logged in first!</h3>
    </asp:Panel>
</body>
</html>
